from contexts.SellContext import SellContext
from goods.Cake import Cake
from goods.Coffee import Coffee
from strategies.CakeSellStrategy import CakeSellStrategy
from strategies.CoffeeSellStrategy import CoffeeSellStrategy

profit = 0

cake = Cake(_id="1", name="aga", shelf_life=10, components=["kek", "lol"],
            quantity=1, price=100, manufacturer="Aga Inc.", additional_tires=2)

coffee = Coffee(_id="2", name="lol", shelf_life=1, components=["aga", "sho"],
                quantity=1, price=45, manufacturer="Sho Inc.")

coffee.describe()
context = SellContext(CoffeeSellStrategy())
profit += context.sell(price=coffee.price, is_teen_allowed=coffee.is_teen_allowed)

# cake.describe()
# context = SellContext(CakeSellStrategy())
# profit += context.sell(price=cake.price, additional_tires=cake.additional_tires)

print(profit)
