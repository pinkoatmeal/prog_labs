from abc import ABC, abstractmethod


class SellStrategy(ABC):

    @abstractmethod
    def sell(self, price, **kwargs):
        pass
