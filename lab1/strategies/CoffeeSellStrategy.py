from exceptions.SellError import SellError
from strategies.SellStrategy import SellStrategy


class CoffeeSellStrategy(SellStrategy):
    def sell(self, price, **kwargs):
        try:
            is_teen_allowed = kwargs["is_teen_allowed"]
        except KeyError:
            return price
        if not is_teen_allowed:
            raise SellError("Whiskey in coffee is not allowed for teens")
        else:
            return price
