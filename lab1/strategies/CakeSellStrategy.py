from strategies.SellStrategy import SellStrategy


class CakeSellStrategy(SellStrategy):
    def sell(self, price, **kwargs):
        return price + kwargs["additional_tires"] * 75 if "additional_tires" in kwargs.keys() else price
