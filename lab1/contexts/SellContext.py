from strategies import SellStrategy


class SellContext:
    def __init__(self, strategy: SellStrategy):
        self._strategy: SellStrategy = strategy

    @property
    def strategy(self) -> SellStrategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy) -> None:
        self._strategy = strategy

    def sell(self, price, **kwargs) -> float:
        return self._strategy.sell(price, **kwargs)
