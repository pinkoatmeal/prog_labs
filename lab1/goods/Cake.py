from goods.AbstractGood import AbstractGood


class Cake(AbstractGood):
    def __init__(self, _id, name, shelf_life, components, quantity, price, manufacturer, additional_tires=0):
        super().__init__(_id, name, shelf_life, components, quantity, price, manufacturer)
        self.additional_tires = additional_tires

    def read_from_db(self):
        pass

    def commit_to_db(self):
        pass

    def add(self):
        pass

    def delete(self):
        pass
