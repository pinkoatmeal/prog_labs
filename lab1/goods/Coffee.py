from goods.AbstractGood import AbstractGood


class Coffee(AbstractGood):
    def __init__(self, _id, name, shelf_life, components, quantity, price, manufacturer):
        super().__init__(_id, name, shelf_life, components, quantity, price, manufacturer)
        self.is_teen_allowed: bool = False if "whiskey" in components else True

    def read_from_db(self):
        pass

    def commit_to_db(self):
        pass

    def add(self):
        pass

    def delete(self):
        pass
