from abc import ABC, abstractmethod
from typing import List


class AbstractGood(ABC):
    def __init__(self, _id, name, shelf_life, components, quantity, price, manufacturer):
        self.id: str = _id
        self.name: str = name
        self.shelf_life: float = shelf_life
        self.components: List[str] = list(map(lambda item: item.lower(), components))
        self.quantity: int = quantity
        self.price: float = price
        self.manufacturer: str = manufacturer

    @abstractmethod
    def read_from_db(self):
        pass

    @abstractmethod
    def commit_to_db(self):
        pass

    @abstractmethod
    def add(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    def describe(self):
        print(f"id={self.id}, {self.name=}, {self.shelf_life=}, {self.components=}, "
              f"{self.quantity=}, {self.price=}, {self.manufacturer=}.")
